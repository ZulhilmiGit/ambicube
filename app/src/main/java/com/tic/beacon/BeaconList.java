package com.tic.beacon;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BeaconList extends AppCompatActivity implements
        BeaconAdapter.onClickListener {

    private static final String TAG = BeaconList.class.getSimpleName();


    final Context context = this;
    private BluetoothView bluetoothView;
    private BeaconAdapter adapter;
    private List<Beacons> list;
    private BluetoothAdapter bt_Adapter;
    private BluetoothLeScanner scanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private Map<String, Beacons> maps = new HashMap<>();

    private Location location;

    private ProgressDialog pd;
    private View login_progress;
    TextView emptyBeaconList;

    private ScanCallback scanCallback = new ScanCallback() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            if (result.getScanRecord() != null) {
                final byte[] scanRecord = result.getScanRecord().getBytes();
                final byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, 2 + 4, uuidBytes, 0, 16);
                final UUID uid = Utils.bytesToUuid(uuidBytes);
                for (int i = 0; i < scanRecord.length; i++) {
                    int payloadLength = Utils.unsignedByteToInt(scanRecord[i]);

                    if ((payloadLength == 0) || (i + 1 >= scanRecord.length)) {
                        break;
                    }

                    if (Utils.unsignedByteToInt(scanRecord[(i + 1)]) != 255) {
                        i += payloadLength;
                        final Beacons beacon = new Beacons();
                        String name = result.getDevice().getName();
                        if (name == null || name.isEmpty()) {
                            name = "Peripherals";
                        }
                    } else {
                        byte[] uuidByte = new byte[16];
                        System.arraycopy(scanRecord, 2 + 7, uuidByte, 0, 16);
                        final UUID uuid = Utils.bytesToUuid(uuidByte);
                        Log.e(TAG, "UUID 16: " + result.getDevice().getName() + ",\n" + uuid.toString());
                        if (payloadLength == 26 || payloadLength == 27) {
                            if ((Utils.unsignedByteToInt(scanRecord[(i + 2)]) == 76)
                                    && (Utils.unsignedByteToInt(scanRecord[(i + 3)]) == 0)
                                    && (Utils.unsignedByteToInt(scanRecord[(i + 4)]) == 2)
                                    && ((Utils.unsignedByteToInt(scanRecord[(i + 5)]) == 21)
                                    || (Utils.unsignedByteToInt(scanRecord[(i + 5)]) == 22))) {


                                int major = Utils.unsignedByteToInt(scanRecord[(i + 22)])
                                        * 256 + Utils.unsignedByteToInt(scanRecord[(i + 23)]);
                                int minor = Utils.unsignedByteToInt(scanRecord[(i + 24)])
                                        * 256 + Utils.unsignedByteToInt(scanRecord[(i + 25)]);
                                int measuredPower = scanRecord[(i + 26)];

                                if (payloadLength == 27) {
                                    int BattLevel = scanRecord[(i + 27)];
                                    final Beacons beacon = new Beacons();

                                    try {
                                        if (result.getDevice().getName().equals("AmbiCube")) {
                                            beacon.setName("AmbiCube");
                                            beacon.setLocation(String.valueOf(location));
                                            beacon.setMac(result.getDevice().getAddress());
                                            beacon.setDistance(Utils.computeAccuracy(result.getRssi(), measuredPower));
                                            beacon.setUuid(uuid.toString());
                                            beacon.setRssi(String.valueOf(result.getRssi()));
                                            beacon.setPower(String.valueOf(measuredPower));
                                            beacon.setBattery(String.valueOf(BattLevel));
                                            beacon.setMajor(String.valueOf(major));
                                            beacon.setMinor(String.valueOf(minor));
                                            beacon.setTemperature(Utils.getTemp(major, minor));
                                            beacon.setHumidity(Utils.getHum(major));
                                            if (!maps.containsKey(result.getDevice().getAddress())) {
                                                maps.put(result.getDevice().getAddress(), beacon);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.e("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "scanCallback onScanFailed with error" + errorCode);
            emptyBeaconList.setText("No Beacon Found");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Utils.isBleSupported(this)) {
            showdialog("BLUETOOTH BLE NOT FOUND", "THIS DEVICE DOES NOT SUPPORT BLUETOOTH BLE!");
            finish();
        }

        BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bt_Adapter = btManager.getAdapter();
        scanner = bt_Adapter.getBluetoothLeScanner();
        settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
        filters = new ArrayList<>();

        setContentView(R.layout.beacon_list);

        login_progress = findViewById(R.id.login_progress);
        bluetoothView = findViewById(R.id.bluetooth_view);
        emptyBeaconList = findViewById(R.id.emptyBeaconList);

        RecyclerView recyclerView = findViewById(R.id.beacon_rc);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<>();
        adapter = new BeaconAdapter(this, list, this);
        recyclerView.setAdapter(adapter);

        pd = new ProgressDialog(this);

        checkBluetoothConnection();
        Utils.onGPS(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Utils.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, BeaconList.class));
                finish();
            } else {
                checkBluetoothConnection();
            }
        } else if (requestCode == Utils.REQUEST_ENABLE_LOCATION) {
            if (resultCode != RESULT_OK) {
                Utils.onGPS(this);
            }
        }
    }

    @Override
    public void onConnectClick(int position) {

    }
    @Override
    public void onDataClick(int position) {

    }

    //Open up dialog box allowing user to enter a beacon name and save the beacon name, mac, and uuid to sharedPreference
    @Override
    public void onAddClick(int position) {

        LayoutInflater li = LayoutInflater.from(context);
        View promptview = li.inflate(R.layout.promptview, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptview);
        final String MAC = list.get(position).getMac();
        final String UUID = list.get(position).getUuid();

        final TextView mac = promptview.findViewById(R.id.beacon_mac);
        mac.setText("MAC : " + MAC);


        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Dialog f = (Dialog) dialog;
                                EditText nameinput;

                                nameinput = f.findViewById(R.id.name_input);
                                String name = nameinput.getText().toString();

                                MainActivity.addBarrel(name, MAC, UUID);
                                MainActivity.saveData(getApplicationContext());

                                f.dismiss();

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.show();

    }

    private void checkBluetoothConnection() {
        if (bt_Adapter == null || !bt_Adapter.isEnabled() && bluetoothView != null) {
            bluetoothView.setEnable(false);
            bluetoothView.setTitle(getString(R.string.disconnect));
            bluetoothView.refreshBG();
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), Utils.REQUEST_ENABLE_BT);
        } else {
            if (bluetoothView != null) {
                bluetoothView.setEnable(true);
                bluetoothView.setTitle(getString(R.string.connect));
                bluetoothView.refreshBG();
                scanDevices(true);
            }
        }
    }

    private void scanDevices(final boolean enable) {
        if (enable) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    scanner.stopScan(scanCallback);
                    showProgress(false);
                    list.addAll(maps.values());
                    adapter.notifyDataSetChanged();
                }
            }, 10 * 1000);
            showProgress(true);
            scanner.startScan(filters, settings, scanCallback);
        } else {
            showProgress(false);
            scanner.stopScan(scanCallback);
        }

    }

    private void showProgress(final boolean show) {
        login_progress.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    private void showdialog(final String title, final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog ad = new AlertDialog.Builder(BeaconList.this)
                        .setTitle(title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                ad.show();
            }
        });

    }

    interface BluetoothService {
        void update(BluetoothGattCharacteristic paramBluetoothGattCharacteristic);

        void onCharacteristicWrite(BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int state);
    }

    interface ConnectionCallback {
        void onAuthenticated(Beacons Beacons);

        void onAuthenticationError();

        void onDisconnected();

        void onSensorData(byte[] FlashData);
    }
}
