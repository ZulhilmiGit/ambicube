package com.tic.beacon;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FullScreenActivity extends AppCompatActivity {

    String result;
    String graphBtn;
    String activityName;
    String weekNum;

    static ArrayList<Date> list = new ArrayList<>();
    static ArrayList<TempHumid> yearTempHumids = HistoryYearActivity.getAll();
    static ArrayList<TempHumid> monthTempHumids = HistoryMonthActivity.getAll();
    static ArrayList<TempHumid> allTempHumids = HistoryActivity.getAll();
    private static final String TAG = FullScreenActivity.class.getSimpleName();
    ArrayList<TempHumid> WeekData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_full_screen_overall);

        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        graphBtn = intent.getStringExtra("graphBtn");
        activityName = intent.getStringExtra("activityName");
        weekNum = intent.getStringExtra("weekNum");

        final GraphView graph = findViewById(R.id.overall_graph);

        //parse different Object ArrayList based on previous page and which graph is being fullscreen
        if(graphBtn.equalsIgnoreCase("temperature"))
        {
            try {
                if(activityName.equalsIgnoreCase("HistoryYearActivity"))
                {
                    parseToGraphView(yearTempHumids, graph, "temperature");
                }
                else if(activityName.equalsIgnoreCase("HistoryMonthActivity"))
                {
                    parseToGraphView(monthTempHumids, graph, "temperature");
                }
                else if(activityName.equalsIgnoreCase("HistoryActivity"))
                {
                    //fullscreen different data based on which week is selected.
                    if(weekNum.equalsIgnoreCase("reset"))
                    {
                        parseToGraphView(allTempHumids, graph, "temperature");
                    }
                    else if(weekNum.equalsIgnoreCase("1"))
                    {
                        parseToGraphView2(allTempHumids, graph, "temperature","1");
                    }
                    else if(weekNum.equalsIgnoreCase("2"))
                    {
                        parseToGraphView2(allTempHumids, graph, "temperature","2");
                    }
                    else if(weekNum.equalsIgnoreCase("3"))
                    {
                        parseToGraphView2(allTempHumids, graph, "temperature","3");
                    }
                    else if(weekNum.equalsIgnoreCase("4"))
                    {
                        parseToGraphView2(allTempHumids, graph, "temperature","4");
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (graphBtn.equalsIgnoreCase("humidity"))
        {
            try {
                if(activityName.equalsIgnoreCase("HistoryYearActivity"))
                {
                    parseToGraphView(yearTempHumids, graph, "humidity");
                }
                else if(activityName.equalsIgnoreCase("HistoryMonthActivity"))
                {
                    parseToGraphView(monthTempHumids, graph, "humidity");
                }
                else if(activityName.equalsIgnoreCase("HistoryActivity"))
                {
                    if(weekNum.equalsIgnoreCase("reset"))
                    {
                        parseToGraphView(allTempHumids, graph, "humidity");
                    }
                    else if(weekNum.equalsIgnoreCase("1"))
                    {
                        parseToGraphView2(allTempHumids, graph, "humidity","1");
                    }
                    else if(weekNum.equalsIgnoreCase("2"))
                    {
                        parseToGraphView2(allTempHumids, graph, "humidity","2");
                    }
                    else if(weekNum.equalsIgnoreCase("3"))
                    {
                        parseToGraphView2(allTempHumids, graph, "humidity","3");
                    }
                    else if(weekNum.equalsIgnoreCase("4"))
                    {
                        parseToGraphView2(allTempHumids, graph, "humidity","4");
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void parseToGraphView(ArrayList<TempHumid> X, GraphView graph, final String graphName) throws ParseException {

        String date1 = X.get(0).getCalendar().replaceAll("-","/");
        String date2 = X.get(X.size() - 1).getCalendar().replaceAll("-","/");

        Date d1 = new SimpleDateFormat("yyyy/MM/dd").parse(date1);
        Date d3 = new SimpleDateFormat("yyyy/MM/dd").parse(date2);

        DataPoint[] dp = new DataPoint[X.size()];
        for(int i= 0; i < X.size(); i++){
            Date dat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(X.get(i).getCalendar().replaceAll("-","/"));
            Double tem = Double.valueOf(X.get(i).getTemperature());
            Double hum = Double.valueOf(X.get(i).getHumidity());

            if(graphName.equalsIgnoreCase("temperature"))
            {
                dp[i] = new DataPoint(dat, tem);
            }
            else if (graphName.equalsIgnoreCase("humidity"))
            {
                dp[i] = new DataPoint(dat, hum);
            }
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);

        if(graphName.equalsIgnoreCase("temperature"))
        {
            series.setColor(Color.BLUE);
            series.setTitle("°C");
        }
        else if (graphName.equalsIgnoreCase("humidity"))
        {
            series.setColor(Color.RED);
            series.setTitle("%");
        }

        graph.addSeries(series);

        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setMargin(30);

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));

        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d3.getTime());
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(7f);
        graph.getGridLabelRenderer().setHumanRounding(false, true);

        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Double pointY = dataPoint.getY();
                SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String pointX = SDF.format(dataPoint.getX());
                if(graphName.equalsIgnoreCase("temperature"))
                {
                    Toast.makeText(FullScreenActivity.this, "Date & Time : " + pointX + '\n' + "Temperature : " + pointY, Toast.LENGTH_LONG).show();
                }
                else if (graphName.equalsIgnoreCase("humidity"))
                {
                    Toast.makeText(FullScreenActivity.this, "Date & Time : " + pointX + '\n' + "Humidity : " + pointY, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void parseToGraphView2(ArrayList<TempHumid> X, GraphView graph, final String graphName, String Week) throws ParseException {

        for (int i = 0; i < X.size(); i++) {

            String week = X.get(i).getWeek();

            //there is week 0 for some months, so we just add that as week 1
            if(week.equalsIgnoreCase("0") && Week.equalsIgnoreCase("1"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("1") && Week.equalsIgnoreCase("1"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("2") && Week.equalsIgnoreCase("2"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("3") && Week.equalsIgnoreCase("3"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("4") && Week.equalsIgnoreCase("4"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            //there is week 5 for some months, so we just add that as week 4
            else if(week.equalsIgnoreCase("5") && Week.equalsIgnoreCase("4"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
        }

        Collections.sort(WeekData);

        String date1 = X.get(0).getCalendar().replaceAll("-", "/");
        String date2 = X.get(X.size() - 1).getCalendar().replaceAll("-", "/");

        Date d1 = new java.text.SimpleDateFormat("yyyy/MM/dd").parse(date1);
        Date d3 = new java.text.SimpleDateFormat("yyyy/MM/dd").parse(date2);

        DataPoint[] dp = new DataPoint[WeekData.size()];
        for(int h = 0; h < WeekData.size(); h++)
        {
            Date dat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(WeekData.get(h).getCalendar().replaceAll("-", "/"));

            Double tem = Double.valueOf(WeekData.get(h).getTemperature());
            Double hum = Double.valueOf(WeekData.get(h).getHumidity());

            if(graphName.equalsIgnoreCase("temperature"))
            {
                dp[h] = new DataPoint(dat, tem);
            }
            else if (graphName.equalsIgnoreCase("humidity"))
            {
                dp[h] = new DataPoint(dat, hum);
            }
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);

        if(graphName.equalsIgnoreCase("temperature"))
        {
            series.setColor(Color.BLUE);
            series.setTitle("°C");
        }
        else if (graphName.equalsIgnoreCase("humidity"))
        {
            series.setColor(Color.RED);
            series.setTitle("%");
        }

        graph.addSeries(series);

        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setMargin(30);

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));

        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d3.getTime());
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(7f);
        graph.getGridLabelRenderer().setHumanRounding(false, true);

        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Double pointY = dataPoint.getY();
                SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String pointX = SDF.format(dataPoint.getX());
                if(graphName.equalsIgnoreCase("temperature"))
                {
                    Toast.makeText(FullScreenActivity.this, "Date & Time : " + pointX + '\n' + "Temperature : " + pointY, Toast.LENGTH_LONG).show();
                }
                else if (graphName.equalsIgnoreCase("humidity"))
                {
                    Toast.makeText(FullScreenActivity.this, "Date & Time : " + pointX + '\n' + "Humidity : " + pointY, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void addToMap(String date, String temp, String humid, String Week) {

        TempHumid tempHumid = new TempHumid();
        Map<String, TempHumid> tempHumidMaps = new HashMap<>();

        tempHumid.setCalendar(date);
        tempHumid.setTemperature(temp);
        tempHumid.setHumidity(humid);
        tempHumid.setWeek(Week);

        if(!tempHumidMaps.containsKey(date))
        {
            tempHumidMaps.put(date, tempHumid);
        }

        WeekData.addAll(tempHumidMaps.values());
    }
}
