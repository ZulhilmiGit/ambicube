package com.tic.beacon;

import android.bluetooth.BluetoothDevice;

class Beacons {

    private String name;
    private String mac;
    private String location;
    private String rssi;
    private String power;
    private String battery;
    private String major;
    private String minor;
    private String temperature;
    private String humidity;
    private String uuid;
    private String distance;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Beacons() {
    }

    public Beacons(JaaleeService jl, BluetoothDevice device){
        this.mac = device.getAddress();
        this.name = device.getName();
        this.uuid = jl.getBeaconUUID();
        this.major = String.valueOf(jl.getBeaconMajor());
        this.minor = String.valueOf(jl.getBeaconMinor());
        this.power = String.valueOf(jl.getBeaconPower());
    }

}
