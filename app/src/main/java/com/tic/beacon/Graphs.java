package com.tic.beacon;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Graphs extends AppCompatActivity {

    String result;
    String Year;
    String Month;
    String activityName;
    String weekNum = "reset";
    String barrelName;

    ArrayList<TempHumid> yearTempHumids = HistoryYearActivity.getAll();
    ArrayList<TempHumid> monthTempHumids = HistoryMonthActivity.getAll();
    ArrayList<TempHumid> allTempHumids = HistoryActivity.getAll();
    private static final String TAG = Graphs.class.getSimpleName();
    ArrayList<TempHumid> WeekData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphs);

        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        activityName = intent.getStringExtra("activityName");
        Year = intent.getStringExtra("Year");
        Month = intent.getStringExtra("Month");
        barrelName = intent.getStringExtra("barrelName");
        TextView textView = findViewById(R.id.textView);
        final GraphView temp_graph = findViewById(R.id.temp_graph);
        final GraphView humid_graph = findViewById(R.id.humid_graph);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //parse different Object ArrayList based on the previous page
        try {
            if(activityName.equalsIgnoreCase("HistoryYearActivity"))
            {
                textView.setText("Overall Graphs");
                parseToGraphView(yearTempHumids, temp_graph, "temperature");
                parseToGraphView(yearTempHumids, humid_graph, "humidity");
            }
            else if(activityName.equalsIgnoreCase("HistoryMonthActivity"))
            {
                textView.setText(Year+" Graphs");
                parseToGraphView(monthTempHumids, temp_graph, "temperature");
                parseToGraphView(monthTempHumids, humid_graph, "humidity");
            }
            else if(activityName.equalsIgnoreCase("HistoryActivity"))
            {
                textView.setText(barrelName + '\n' + Month+" "+Year+" Graphs");
                parseToGraphView(allTempHumids, temp_graph, "temperature");
                parseToGraphView(allTempHumids, humid_graph, "humidity");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //open up FullScreenActivity for temperature
        ImageButton imgBtn = findViewById(R.id.fullscreen);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Graphs.this, FullScreenActivity.class);
                myIntent.putExtra("result", result);
                myIntent.putExtra("graphBtn", "temperature");
                myIntent.putExtra("activityName", activityName);
                myIntent.putExtra("weekNum", weekNum);
                startActivity(myIntent);
            }
        });

        //open up FullScreenActivity for humidity
        ImageButton imgBtn2 = findViewById(R.id.fullscreen2);
        imgBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Graphs.this, FullScreenActivity.class);
                myIntent.putExtra("result", result);
                myIntent.putExtra("graphBtn", "humidity");
                myIntent.putExtra("activityName", activityName);
                myIntent.putExtra("weekNum", weekNum);
                startActivity(myIntent);
            }
        });
    }

    //hide menu from HistoryYearActivity and HistoryMonthActivity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(activityName.equalsIgnoreCase("HistoryYearActivity") || activityName.equalsIgnoreCase("HistoryMonthActivity"))
        {
            return false;
        }
        else
        {
            getMenuInflater().inflate(R.menu.main_menu, menu);
        }
        return true;
    }

    //handle click on option items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final GraphView temp_graph = findViewById(R.id.temp_graph);
        final GraphView humid_graph = findViewById(R.id.humid_graph);

        switch (item.getItemId()) {
            case R.id.overall:
                //reset the activity
                weekNum = "reset";
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
                return true;
            case R.id.week_1:
                //weekNum is the week of the month that user wish to see
                weekNum = "1";
                //clear Object ArrayList and graphs before parsing filtered data
                WeekData.clear();
                temp_graph.removeAllSeries();
                humid_graph.removeAllSeries();
                try {
                    //parse filtered data
                    parseToGraphView2(allTempHumids, temp_graph, "temperature", "1");
                    parseToGraphView2(allTempHumids, humid_graph, "humidity", "1");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.week_2:
                weekNum = "2";
                WeekData.clear();
                temp_graph.removeAllSeries();
                humid_graph.removeAllSeries();
                try {
                    parseToGraphView2(allTempHumids, temp_graph, "temperature", "2");
                    parseToGraphView2(allTempHumids, humid_graph, "humidity", "2");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.week_3:
                weekNum = "3";
                WeekData.clear();
                temp_graph.removeAllSeries();
                humid_graph.removeAllSeries();
                try {
                    parseToGraphView2(allTempHumids, temp_graph, "temperature", "3");
                    parseToGraphView2(allTempHumids, humid_graph, "humidity", "3");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.week_4:
                weekNum = "4";
                WeekData.clear();
                temp_graph.removeAllSeries();
                humid_graph.removeAllSeries();
                try {
                    parseToGraphView2(allTempHumids, temp_graph, "temperature", "4");
                    parseToGraphView2(allTempHumids, humid_graph, "humidity", "4");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //parse all data of the month to the graphs
    public void parseToGraphView(ArrayList<TempHumid> X, GraphView graph, String graphName) throws ParseException {

        //getting first and last date in Object ArrayList
        String date1 = X.get(0).getCalendar().replaceAll("-","/");
        String date2 = X.get(X.size() - 1).getCalendar().replaceAll("-","/");

        //Convert date1 and date2 to Date format
        Date d1 = new SimpleDateFormat("yyyy/MM/dd").parse(date1);
        Date d3 = new SimpleDateFormat("yyyy/MM/dd").parse(date2);

        //initialize amount of data points based on Object ArrayList size
        DataPoint[] dp = new DataPoint[X.size()];
        for(int i= 0; i < X.size(); i++){
            Date dat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(X.get(i).getCalendar().replaceAll("-","/"));

            //get temperature and humidity value
            Double tem = Double.valueOf(X.get(i).getTemperature());
            Double hum = Double.valueOf(X.get(i).getHumidity());

            //parse temperature and humidity to separate graphs
            if(graphName.equalsIgnoreCase("temperature"))
            {
                dp[i] = new DataPoint(dat, tem);
            }
            else if (graphName.equalsIgnoreCase("humidity"))
            {
                dp[i] = new DataPoint(dat, hum);
            }
        }

        //initialize line graph with data points
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);

        //set line color and add legend
        if(graphName.equalsIgnoreCase("temperature"))
        {
            series.setColor(Color.BLUE);
            series.setTitle("°C");
        }
        else if (graphName.equalsIgnoreCase("humidity"))
        {
            series.setColor(Color.RED);
            series.setTitle("%");
        }

        graph.addSeries(series);

        //render legend on graph
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setMargin(30);

        //set amount of date viewable on X axis
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graph.getGridLabelRenderer().setNumHorizontalLabels(2);

        //set start and end date on X axis
        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d3.getTime());
        graph.getViewport().setXAxisBoundsManual(true);
        //format the date to readable format
        graph.getGridLabelRenderer().setHumanRounding(false, true);
    }

    //only parse data according to selected week
    public void parseToGraphView2(ArrayList<TempHumid> X, GraphView graph, String graphName, String Week) throws ParseException {

        for (int i = 0; i < X.size(); i++) {

            String week = X.get(i).getWeek();

            //only add data to graph based on week of month
            if(week.equalsIgnoreCase("0") && Week.equalsIgnoreCase("1"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                //add to the
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("1") && Week.equalsIgnoreCase("1"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("2") && Week.equalsIgnoreCase("2"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("3") && Week.equalsIgnoreCase("3"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("4") && Week.equalsIgnoreCase("4"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
            else if(week.equalsIgnoreCase("5") && Week.equalsIgnoreCase("4"))
            {
                String DATE = X.get(i).getCalendar();
                String TEMP = X.get(i).getTemperature();
                String HUM = X.get(i).getHumidity();
                addToMap(DATE,TEMP,HUM,week);
            }
        }

        //sort Object ArrayList in ascending order
        Collections.sort(WeekData);

        String date1 = X.get(0).getCalendar().replaceAll("-", "/");
        String date2 = X.get(X.size() - 1).getCalendar().replaceAll("-", "/");

        Date d1 = new SimpleDateFormat("yyyy/MM/dd").parse(date1);
        Date d3 = new SimpleDateFormat("yyyy/MM/dd").parse(date2);

        DataPoint[] dp = new DataPoint[WeekData.size()];
        for(int h = 0; h < WeekData.size(); h++)
        {
            Date dat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(WeekData.get(h).getCalendar().replaceAll("-", "/"));

            Double tem = Double.valueOf(WeekData.get(h).getTemperature());
            Double hum = Double.valueOf(WeekData.get(h).getHumidity());

            if(graphName.equalsIgnoreCase("temperature"))
            {
                dp[h] = new DataPoint(dat, tem);
            }
            else if (graphName.equalsIgnoreCase("humidity"))
            {
                dp[h] = new DataPoint(dat, hum);
            }
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);

        if(graphName.equalsIgnoreCase("temperature"))
        {
            series.setColor(Color.BLUE);
            series.setTitle("°C");
        }
        else if (graphName.equalsIgnoreCase("humidity"))
        {
            series.setColor(Color.RED);
            series.setTitle("%");
        }

        graph.addSeries(series);

        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setMargin(30);

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graph.getGridLabelRenderer().setNumHorizontalLabels(2);

        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d3.getTime());
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getGridLabelRenderer().setHumanRounding(false, true);
    }

    //add data to Object ArrayList. HashMap used to map data to date
    public void addToMap(String date, String temp, String humid, String Week) {

        TempHumid tempHumid = new TempHumid();
        Map<String, TempHumid> tempHumidMaps = new HashMap<>();

        tempHumid.setCalendar(date);
        tempHumid.setTemperature(temp);
        tempHumid.setHumidity(humid);
        tempHumid.setWeek(Week);

        if(!tempHumidMaps.containsKey(date))
        {
            tempHumidMaps.put(date, tempHumid);
        }

        WeekData.addAll(tempHumidMaps.values());
    }

}
