package com.tic.beacon;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


class uploadDB extends AsyncTask<String, Void, String> {

    private final String TAG = uploadDB.class.getSimpleName();

    private returnResult returnResult = null;

    uploadDB(  returnResult returnResult){

        this.returnResult = returnResult;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String method = params[0];
        String data = "";
        String link = "http://www.wmsecure.com.sg/gateway.php";

        switch (method){
            case "ping": {
                try{
                    data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("ping", "UTF-8");
                    Log.e(TAG,"DATA: " + data);
                }catch (UnsupportedEncodingException e){
                    Log.e(TAG, "UnsupportedEncodingException: " + e.toString());
                }
                try {
                    URL url = new URL(link);
                    HttpURLConnection h = (HttpURLConnection) url.openConnection();
                    h.setDoInput(true);
                    h.setDoOutput(true);
                    OutputStreamWriter owriter =
                            new OutputStreamWriter(h.getOutputStream());
                    owriter.write(data);
                    owriter.flush();
                    BufferedReader b = new BufferedReader
                            (new InputStreamReader(h.getInputStream(), "UTF-8"), 8);
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = b.readLine()) != null) {
                        builder.append(line);
                    }
                    return builder.toString();
                } catch (IOException e) {
                    //activity.isInternetConnected = false;
                    Log.e(TAG, "IOException: " + e.toString());
                }
            }
            break;
            case "updatecounter": {
                String macAddress = params[1];
                String sensorname = params[2];
                String counter = params[3];
                try {
                    data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("counter", "UTF-8")
                            + "&" + URLEncoder.encode("gate_name", "UTF-8") + "=" + URLEncoder.encode(sensorname, "UTF-8")
                            + "&" + URLEncoder.encode("mac", "UTF-8") + "=" + URLEncoder.encode(macAddress, "UTF-8")
                            + "&" + URLEncoder.encode("counter", "UTF-8") + "=" + URLEncoder.encode(counter, "UTF-8");
                    Log.e(TAG,"DATA: " + data);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException: " + e.toString());
                }

                try {
                    URL url = new URL(link);
                    HttpURLConnection h = (HttpURLConnection) url.openConnection();
                    h.setDoInput(true);
                    h.setDoOutput(true);
                    OutputStreamWriter owriter =
                            new OutputStreamWriter(h.getOutputStream());
                    owriter.write(data);
                    owriter.flush();
                    BufferedReader b = new BufferedReader
                            (new InputStreamReader(h.getInputStream(), "UTF-8"), 8);
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = b.readLine()) != null) {
                        builder.append(line);
                    }
                    return builder.toString();
                } catch (IOException e) {
                    //activity.isInternetConnected = false;
                    Log.e(TAG, "IOException: " + e.toString());
                }
            }
            break;
            case "updatedb":{
                String macAddress = params[1];
                String sensorname = params[2];
                String temperature1 = params[3];
                String humidity1 = params[4];
                String temperature2 = params[5];
                String humidity2 = params[6];
                String temperature3 = params[7];
                String humidity3 = params[8];
                String temperature4 = params[9];
                String humidity4 = params[10];
                String temperature5 = params[11];
                String humidity5 = params[12];
                String uuid = params[13];
                String geo_lat = params[14];
                String geo_lng = params[15];

                try {
                    data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("gateway", "UTF-8")
                            + "&" + URLEncoder.encode("gate_name", "UTF-8") + "=" + URLEncoder.encode(sensorname, "UTF-8")
                            + "&" + URLEncoder.encode("mac", "UTF-8") + "=" + URLEncoder.encode(macAddress, "UTF-8")
                            + "&" + URLEncoder.encode("temp_sensor1", "UTF-8") + "=" + URLEncoder.encode(temperature1, "UTF-8")
                            + "&" + URLEncoder.encode("hum_sensor1", "UTF-8") + "=" + URLEncoder.encode(humidity1, "UTF-8")
                            + "&" + URLEncoder.encode("temp_sensor2", "UTF-8") + "=" + URLEncoder.encode(temperature2, "UTF-8")
                            + "&" + URLEncoder.encode("hum_sensor2", "UTF-8") + "=" + URLEncoder.encode(humidity2, "UTF-8")
                            + "&" + URLEncoder.encode("temp_sensor3", "UTF-8") + "=" + URLEncoder.encode(temperature3, "UTF-8")
                            + "&" + URLEncoder.encode("hum_sensor3", "UTF-8") + "=" + URLEncoder.encode(humidity3, "UTF-8")
                            + "&" + URLEncoder.encode("temp_sensor4", "UTF-8") + "=" + URLEncoder.encode(temperature4, "UTF-8")
                            + "&" + URLEncoder.encode("hum_sensor4", "UTF-8") + "=" + URLEncoder.encode(humidity4, "UTF-8")
                            + "&" + URLEncoder.encode("temp_sensor5", "UTF-8") + "=" + URLEncoder.encode(temperature5, "UTF-8")
                            + "&" + URLEncoder.encode("hum_sensor5", "UTF-8") + "=" + URLEncoder.encode(humidity5, "UTF-8")
                            + "&" + URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(uuid, "UTF-8")
                            + "&" + URLEncoder.encode("geo_lat", "UTF-8") + "=" + URLEncoder.encode(geo_lat, "UTF-8")
                            + "&" + URLEncoder.encode("geo_lng", "UTF-8") + "=" + URLEncoder.encode(geo_lng, "UTF-8");
                    Log.e(TAG,"DATA: " + data);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException: " + e.toString());
                }

                try {
                    URL url = new URL(link);
                    HttpURLConnection h = (HttpURLConnection) url.openConnection();
                    h.setDoInput(true);
                    h.setDoOutput(true);
                    OutputStreamWriter owriter =
                            new OutputStreamWriter(h.getOutputStream());
                    owriter.write(data);
                    owriter.flush();
                    BufferedReader b = new BufferedReader
                            (new InputStreamReader(h.getInputStream(), "UTF-8"), 8);
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = b.readLine()) != null) {
                        builder.append(line);
                    }
                    return builder.toString();
                } catch (IOException e) {
                    //activity.isInternetConnected = false;
                    Log.e(TAG, "IOException: " + e.toString());
                }
            }
            break;
            //function to set offset and limit to amount of history data retrieve from server
            case "historypage":{
                String uuid = params[1];
                String offset = params[2];
                String limit = params[3];
                try {
                    data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("historypage", "UTF-8")
                            + "&" + URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(uuid, "UTF-8")
                            + "&" + URLEncoder.encode("offset", "UTF-8") + "=" + URLEncoder.encode(offset, "UTF-8")
                            + "&" + URLEncoder.encode("limit", "UTF-8") + "=" + URLEncoder.encode(limit, "UTF-8");
                    Log.e(TAG,"DATA: " + data);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG,"UnsupportedEncodingException" + e.toString());
                }

                try {
                    URL url = new URL(link);
                    HttpURLConnection h = (HttpURLConnection) url.openConnection();
                    h.setDoInput(true);
                    h.setDoOutput(true);
                    OutputStreamWriter owriter =
                            new OutputStreamWriter(h.getOutputStream());
                    owriter.write(data);
                    owriter.flush();
                    BufferedReader b = new BufferedReader
                            (new InputStreamReader(h.getInputStream(), "UTF-8"), 8);
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = b.readLine()) != null) {
                        builder.append(line);
                    }
                    return builder.toString();
                } catch (IOException e) {
                    //activity.isInternetConnected = false;
                    Log.e(TAG, "IOException: " + e.toString());
                }
            }
            break;
            //function to get all history data from server
            case "historypageall":{
                String uuid = params[1];
                try {
                    data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("historypageall", "UTF-8")
                            + "&" + URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(uuid, "UTF-8");
                    Log.e(TAG,"DATA: " + data);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG,"UnsupportedEncodingException" + e.toString());
                }

                try {
                    URL url = new URL(link);
                    HttpURLConnection h = (HttpURLConnection) url.openConnection();
                    h.setDoInput(true);
                    h.setDoOutput(true);
                    OutputStreamWriter owriter =
                            new OutputStreamWriter(h.getOutputStream());
                    owriter.write(data);
                    owriter.flush();
                    BufferedReader b = new BufferedReader
                            (new InputStreamReader(h.getInputStream(), "UTF-8"), 8);
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = b.readLine()) != null) {
                        builder.append(line);
                    }
                    return builder.toString();
                } catch (IOException e) {
                    //activity.isInternetConnected = false;
                    Log.e(TAG, "IOException: " + e.toString());
                }
            }
            break;
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        returnResult.onFinish(s);
    }

    interface returnResult{
        void onFinish(String result);
    }
}