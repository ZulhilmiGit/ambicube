package com.tic.beacon;

import android.bluetooth.BluetoothDevice;

//Model class for saved beacon
public class Barrels {

    private String barrelName;
    private String beaconMAC;
    private String beaconUUID;

    public String getBarrelName() {
        return barrelName;
    }

    public void setBarrelName(String barrelName) {
        this.barrelName = barrelName;
    }

    public String getBeaconMAC() {
        return beaconMAC;
    }

    public void setBeaconMAC(String beaconMAC) {
        this.beaconMAC = beaconMAC;
    }

    public String getBeaconUUID() { return beaconUUID; }

    public void setBeaconUUID(String beaconUUID) { this.beaconUUID = beaconUUID; }

    public Barrels() {

    }

    public Barrels(JaaleeServices jl, BluetoothDevice device){
        this.beaconMAC = device.getAddress();
        this.barrelName = device.getName();
        this.beaconUUID = jl.getBeaconUUID();
    }
}
