package com.tic.beacon;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity {

    ListView listView;
    TextView textView;
    JSONArray jsonArray;

    String result;
    String name;
    String Year;
    String Month;
    String uuid;

    private static HistoryAdapter adapter;
    ArrayList<History> records = new ArrayList<>();
    static ArrayList<TempHumid> tempHumido = new ArrayList<>();
    private static final String TAG = HistoryActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);

        listView = findViewById(R.id.listo);
        textView = findViewById(R.id.history_name);

        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        name = intent.getStringExtra("barrelName");
        uuid = intent.getStringExtra("uuid");
        Year = intent.getStringExtra("Year");
        Month = intent.getStringExtra("Month");

        textView.setText(name + '\n' + Month + " " + Year);

        //initialize Load More button on list view
        final Button btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");


        try {
            parsetoListView(result);
            //add Load More button if number of records is > list view count
            if(tempHumido.size() > HistoryAdapter.getItem())
            {
                listView.addFooterView(btnLoadMore);
            }
            HistoryAdapter.resetCount();
            adapter.notifyDataSetChanged();
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                HistoryAdapter.increaseCount();
                adapter.notifyDataSetChanged();
                //remove Load More button list view count is <= number of records
                if(HistoryAdapter.getItem() >= tempHumido.size())
                {
                    listView.removeFooterView(btnLoadMore);
                }
            }
        });
    }

    public void btnGraph(View v)
    {
        Intent intent = new Intent(HistoryActivity.this, Graphs.class);
        intent.putExtra("barrelUUID", uuid);
        intent.putExtra("barrelName", name);
        intent.putExtra("Year", Year);
        intent.putExtra("Month", Month);
        intent.putExtra("result", result);
        intent.putExtra("activityName", "HistoryActivity");
        startActivity(intent);
    }

    public void parsetoListView(String json) throws JSONException, ParseException {

        jsonArray = new JSONArray(json);
        Intent intent = getIntent();
        String monthText = intent.getStringExtra("Month");
        String yearNum = intent.getStringExtra("Year");
        Calendar cal = Calendar.getInstance();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject obj = jsonArray.getJSONObject(i);
            String datetime = obj.getString("created");
            String[] s = datetime.split(" ", 2);
            String dato = datetime.replaceAll("-","/");
            Date pls = new SimpleDateFormat("yyyy/MM/dd").parse(dato);
            cal.setTime(pls);
            int week = cal.get(Calendar.WEEK_OF_MONTH);
            String cb = String.valueOf(week);
            String date = s[0];
            String month = date.substring(5,7);
            String time = s[1];
            String temperature = obj.getString("temp_sensor");
            String humidity = obj.getString("hum_sensor");

            //parse data if the year and month is equals to the ones selected by user
            if(date.contains(yearNum) && monthText.equals("January") && month.equals("01"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("February") && month.equals("02"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("March") && month.equals("03"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("April") && month.equals("04"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("May") && month.equals("05"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("June") && month.equals("06"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("July") && month.equals("07"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("August") && month.equals("08"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("September") && month.equals("09"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("October") && month.equals("10"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("November") && month.equals("11"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
            else if(date.contains(yearNum) && monthText.equals("December") && month.equals("12"))
            {
                records.add(new History(date, time, temperature, humidity));
                addToMap(datetime, temperature, humidity, cb);
            }
        }

        Collections.sort(tempHumido);
        adapter = new HistoryAdapter(records, getApplicationContext());
        listView.setAdapter(adapter);
    }

    public void addToMap(String date, String temp, String humid, String Week) {

        TempHumid tempHumid = new TempHumid();
        Map<String, TempHumid> tempHumidMaps = new HashMap<>();

        tempHumid.setCalendar(date);
        tempHumid.setTemperature(temp);
        tempHumid.setHumidity(humid);
        tempHumid.setWeek(Week);

        if(!tempHumidMaps.containsKey(date))
        {
            tempHumidMaps.put(date, tempHumid);
        }

        tempHumido.addAll(tempHumidMaps.values());
    }

    public static ArrayList<TempHumid> getAll()
    {
        return tempHumido;
    }

    @Override
    public void onBackPressed() {
        tempHumido.clear();
        finish();
    }

}



