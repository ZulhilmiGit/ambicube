package com.tic.beacon;

//Model class for History data
public class History {

    private String Date;
    private String Time;
    private String Temperature;
    private String Humidity;

    public History(String date, String time, String temperature, String humidity) {
        Date = date;
        Time = time;
        Temperature = temperature;
        Humidity = humidity;
    }

    public String getDate() { return Date; }

    //public void setDate(String date) { Date = date; }

    public String getTime() { return Time; }

    //public void setTime(String time) { Time = time; }

    public String getTemperature() { return Temperature; }

    public void setTemperature(String temperature) { Temperature = temperature; }

    public String getHumidity() { return Humidity; }

    public void setHumidity(String humidity) { Humidity = humidity; }
}
