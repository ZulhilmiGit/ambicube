package com.tic.beacon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HistoryMonthActivity extends AppCompatActivity implements
        View.OnClickListener {

    ListView listView;
    JSONArray jsonArray;
    TextView textView;

    String uuid;
    String name;
    String result;
    String Year;
    String Month;

    ArrayList<String> monthList = new ArrayList<>();
    static ArrayList<TempHumid> tempHumido = new ArrayList<>();
    private static final String TAG = HistoryActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_month);

        listView = findViewById(R.id.history_month);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        Year = intent.getStringExtra("Year");
        uuid = intent.getStringExtra("barrelUUID");
        name = intent.getStringExtra("barrelName");
        textView = findViewById(R.id.textView);

        textView.setText(Year);

        findViewById(R.id.graph_view).setOnClickListener(this);

        try {
            parsetoListView(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Month = (String) listView.getItemAtPosition(position);
                Intent myIntent = new Intent(HistoryMonthActivity.this, HistoryActivity.class);
                myIntent.putExtra("Month", Month);
                myIntent.putExtra("Year", Year);
                myIntent.putExtra("barrelUUID", uuid);
                myIntent.putExtra("barrelName", name);
                myIntent.putExtra("result", result);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(HistoryMonthActivity.this, Graphs.class);
        intent.putExtra("barrelUUID", uuid);
        intent.putExtra("barrelName", name);
        intent.putExtra("Year", Year);
        intent.putExtra("result", result);
        intent.putExtra("activityName", "HistoryMonthActivity");
        startActivity(intent);
    }

    public void parsetoListView(String json) throws JSONException {

        jsonArray = new JSONArray(json);
        Intent intent = getIntent();
        String Year = intent.getStringExtra("Year");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            String datetime = obj.getString("created");
            String[] s = datetime.split(" ", 2);
            String date = s[0];
            String tem = obj.getString("temp_sensor");
            String hum = obj.getString("hum_sensor");

            //parse all the month of the year that the user picks (if data in that month exists)
            if(date.contains(Year))
            {
                String month = date.substring(5,7); //get month in date string
                String monthText;
                if(month.equals("01"))
                {
                    monthText = "January"; //set textView accordingly
                    monthList.add(monthText);
                }
                else if(month.equals("02"))
                {
                    monthText = "February";
                    monthList.add(monthText);
                }
                else if(month.equals("03"))
                {
                    monthText = "March";
                    monthList.add(monthText);
                }
                else if(month.equals("04"))
                {
                    monthText = "April";
                    monthList.add(monthText);
                }
                else if(month.equals("05"))
                {
                    monthText = "May";
                    monthList.add(monthText);
                }
                else if(month.equals("06"))
                {
                    monthText = "June";
                    monthList.add(monthText);
                }
                else if(month.equals("07"))
                {
                    monthText = "July";
                    monthList.add(monthText);
                }
                else if(month.equals("08"))
                {
                    monthText = "August";
                    monthList.add(monthText);
                }
                else if(month.equals("09"))
                {
                    monthText = "September";
                    monthList.add(monthText);
                }
                else if(month.equals("10"))
                {
                    monthText = "October";
                    monthList.add(monthText);
                }
                else if(month.equals("11"))
                {
                    monthText = "November";
                    monthList.add(monthText);
                }
                else if (month.equals("12"))
                {
                    monthText = "December";
                    monthList.add(monthText);
                }

                addToMap(datetime, tem, hum);
            }
        }

        Collections.sort(tempHumido);
        Set<String> uniqueMonth = new HashSet<>(monthList);
        List<String> list = new ArrayList<>(uniqueMonth);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(arrayAdapter);
    }

    public void addToMap(String date, String temp, String humid) {

        TempHumid tempHumid = new TempHumid();
        Map<String, TempHumid> tempHumidMaps = new HashMap<>();

        tempHumid.setCalendar(date);
        tempHumid.setTemperature(temp);
        tempHumid.setHumidity(humid);

        if(!tempHumidMaps.containsKey(date))
        {
            tempHumidMaps.put(date, tempHumid);
        }

        tempHumido.addAll(tempHumidMaps.values());
    }

    public static ArrayList<TempHumid> getAll()
    {
        return tempHumido;
    }

    @Override
    public void onBackPressed() {
        tempHumido.clear();
        this.finish();
    }
}
