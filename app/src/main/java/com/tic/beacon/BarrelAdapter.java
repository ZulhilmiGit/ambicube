package com.tic.beacon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

//Custom Recyclerview adapter
class BarrelAdapter extends RecyclerView.Adapter<BarrelAdapter.ViewHolder> {

    private ArrayList<Barrels> list;
    private Context context;
    private onClickListener listener;

    public BarrelAdapter(Context context, ArrayList<Barrels> list, onClickListener listener){
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView card_barrel_name;
        TextView card_beacon_mac;
        TextView card_beacon_uuid;
        Button card_barrel_download;
        Button card_barrel_history;

        //find views in custom card view
        ViewHolder(View v) {
            super(v);
            card_barrel_name = v.findViewById(R.id.card_barrel_name);
            card_beacon_mac = v.findViewById(R.id.card_beacon_mac);
            card_beacon_uuid = v.findViewById(R.id.card_beacon_uuid);
            card_barrel_download = v.findViewById(R.id.card_barrel_download);
            card_barrel_history = v.findViewById(R.id.card_barrel_history);
            card_barrel_download.setOnClickListener(this);
            card_barrel_history.setOnClickListener(this);
        }


        //get adapter position on click
        @Override
        public void onClick(View v) {
            if (v == card_barrel_download) {
                listener.onDownloadClick(getAdapterPosition());
            }
            else if (v == card_barrel_history) {
                listener.onHistoryClick(getAdapterPosition());
            }
        }
    }


    //inflate custom recycler card view
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.barrel_card, parent, false);
        return new ViewHolder(v);
    }


    //bind data to textViews in custom card view
    @Override
    public void onBindViewHolder(ViewHolder h, int position) {
        h.card_barrel_name.setText(list.get(position).getBarrelName());
        h.card_beacon_mac.setText("MAC : " + list.get(position).getBeaconMAC());
        h.card_beacon_uuid.setText("UUID : " + list.get(position).getBeaconUUID());

    }

    //return the total amount of items in Object ArrayList
    @Override
    public int getItemCount() {
        return this.list.size();
    }


    //add click listener
    interface onClickListener {
        void onDownloadClick(int position);
        void onHistoryClick (int position);
    }


}
