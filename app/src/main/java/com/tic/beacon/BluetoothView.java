package com.tic.beacon;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by root on 23-Nov-17.
 */

public class BluetoothView extends RelativeLayout {

    private TextView title = null;
    public RelativeLayout bluetoothbg = null;
    private static boolean enabled = false;

    public BluetoothView(Context context) {
        // TODO Auto-generated constructor stub
        super(context);
    }

    @SuppressWarnings("static-access")
    public BluetoothView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.bluetoothbar, this,
                true);
        title = (TextView) findViewById(R.id.bluetooth_title);
        bluetoothbg = (RelativeLayout) findViewById(R.id.bluetooth_bg);
        bluetoothbg.setEnabled(enabled);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setTitle(int title) {
        this.title.setText(title);
    }

    private void setViewHide(View view, boolean boo) {
        if (boo) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    public void setEnable(boolean boo) {
        bluetoothbg.setEnabled(boo);
        enabled = boo;
    }

    public void refreshBG() {
        bluetoothbg.setEnabled(enabled);
    }
}
