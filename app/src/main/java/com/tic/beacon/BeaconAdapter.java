package com.tic.beacon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class BeaconAdapter extends RecyclerView.Adapter<BeaconAdapter.ViewHolder> {

    private List<Beacons> list;
    private Context context;
    private onClickListener listener;

    BeaconAdapter(Context context, List<Beacons> list, onClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView card_beacon_connect;
        TextView card_beacon_name;
        TextView card_beacon_mac;
        TextView card_beacon_location;
        TextView card_beacon_rssi;
        TextView card_beacon_power;
        TextView card_beacon_battery;
        TextView card_beacon_major;
        TextView card_beacon_temperature;
        TextView card_beacon_minor;
        TextView card_beacon_humidity;
        TextView card_beacon_raw_data;
        TextView card_beacon_uuid;
        Button card_beacon_add;

        ViewHolder(View v) {
            super(v);
            card_beacon_connect = v.findViewById(R.id.card_beacon_connect);
            card_beacon_name = v.findViewById(R.id.card_beacon_name);
            card_beacon_mac = v.findViewById(R.id.card_beacon_mac);
            card_beacon_location = v.findViewById(R.id.card_beacon_location);
            card_beacon_rssi = v.findViewById(R.id.card_beacon_rssi);
            card_beacon_power = v.findViewById(R.id.card_beacon_power);
            card_beacon_battery = v.findViewById(R.id.card_beacon_battery);
            card_beacon_major = v.findViewById(R.id.card_beacon_major);
            card_beacon_temperature = v.findViewById(R.id.card_beacon_temperature);
            card_beacon_minor = v.findViewById(R.id.card_beacon_minor);
            card_beacon_humidity = v.findViewById(R.id.card_beacon_humidity);
            card_beacon_raw_data = v.findViewById(R.id.card_beacon_raw_data);
            card_beacon_uuid = v.findViewById(R.id.card_beacon_uuid);
            card_beacon_add = v.findViewById(R.id.card_beacon_add);
            card_beacon_connect.setOnClickListener(this);
            card_beacon_raw_data.setOnClickListener(this);
            card_beacon_add.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == card_beacon_connect) {
                listener.onConnectClick(getAdapterPosition());

            } else if (v == card_beacon_raw_data) {
                listener.onDataClick(getAdapterPosition());

            } else if (v == card_beacon_add) {
                listener.onAddClick(getAdapterPosition());

            }

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.beacon_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {
        h.card_beacon_name.setText(list.get(position).getName());
        if (!list.get(position).getDistance().equals("0")) {
            h.card_beacon_mac.setText("MAC: " + list.get(position).getMac()
                    + " (" + list.get(position).getDistance() + "m)");
        } else {
            h.card_beacon_mac.setText("MAC: " + list.get(position).getMac());
        }
        //h.card_beacon_location.setText(list.get(position).getLocation());
        h.card_beacon_location.setVisibility(View.GONE);
        h.card_beacon_rssi.setText("RSSI: " + list.get(position).getRssi());
        h.card_beacon_power.setText("MPower: " + list.get(position).getPower());
        h.card_beacon_battery.setText("Battery:" + list.get(position).getBattery() + "%");
        h.card_beacon_major.setText("Major: " + list.get(position).getMajor());
        h.card_beacon_temperature.setText("Temperature: " + list.get(position).getTemperature() + "\u00B0C");
        h.card_beacon_minor.setText("Minor: " + list.get(position).getMinor() + "");
        h.card_beacon_humidity.setText("Humidity: " + list.get(position).getHumidity() + "%");
        h.card_beacon_uuid.setText("UUID: " + list.get(position).getUuid());
    }

    @Override
    public int getItemCount() {
        return this.list == null ? 0 : this.list.size();
    }

    interface onClickListener {
        void onConnectClick(int position);

        void onDataClick(int position);

        void onAddClick(int position);
    }
}
