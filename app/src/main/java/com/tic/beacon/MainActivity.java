package com.tic.beacon;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        LocationListener,
        BarrelAdapter.onClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    private static ArrayList<Barrels> barrelList;
    RecyclerView recyclerView;
    static BarrelAdapter adapter;
    private TextView emptyText;

    private static ProgressDialog pd;
    private BarrelConnection connection;
    private int counter = 0;
    private boolean syncfinished = false;
    private boolean allowed = false;
    private static final String TAG = MainActivity.class.getSimpleName();
    private LocationRequest locationRequest;
    public Location location;
    private GoogleApiClient mGoogleApiClient;
    private BluetoothAdapter bt_Adapter;

    public boolean isInternetConnected;
    public boolean serverConnection;
    private Process p1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        pingServer();

        //Checks for location permission
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Utils.REQUEST_ENABLE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Utils.REQUEST_ENABLE_LOCATION);
            }
        } else {
            allowed = true;

            if (isGooglePlayServicesAvailable()) {
                locationRequest = new LocationRequest();
                locationRequest.setInterval(2 * 1000);
                locationRequest.setFastestInterval(2000);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }
        }

        BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bt_Adapter = btManager.getAdapter();
        findViewById(R.id.toolbar_scan).setOnClickListener(this);
        emptyText = findViewById(R.id.emptyText);
        pd = new ProgressDialog(this);

        recyclerView = findViewById(R.id.barrel_rc);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //load sharedPreference data
        loadData(getApplicationContext());

        adapter = new BarrelAdapter(this, barrelList, this);
        recyclerView.setAdapter(adapter);

        //if there is no saved data in sharedPreference, display the message saying there is no data
        if(adapter.getItemCount() == 0)
        {
            emptyText.setVisibility(View.VISIBLE);
        }
        else
        {
            emptyText.setVisibility(View.GONE);
        }
        checkBluetoothConnection();
        Utils.onGPS(this);
    }

    //check server status
    private void pingServer() {
        new uploadDB(new uploadDB.returnResult() {
            @Override
            public void onFinish(String result) {
                if (result.equalsIgnoreCase("success")) {
                    serverConnection = true;
                } else {
                    serverConnection = false;
                }
            }
        }).execute("ping");
    }

    //check internet connection
    public Boolean isOnline() {
        try {
            p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            if (reachable) {
                isInternetConnected = true;
                return true;
            } else {
                isInternetConnected = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private MainActivity.ConnectionCallback connectionCallback() {
        return new MainActivity.ConnectionCallback() {
            String name, mac, uuid;

            @Override
            public void onAuthenticated(Barrels b) {
                name = "AmbiCube";
                mac = b.getBeaconMAC();
                uuid = b.getBeaconUUID();
                connection.setTemp(true, new BarrelConnection.WriteCallBack() {
                    @Override
                    public void onWriteSuccess() {
                        counter = 0;
                        syncfinished = false;
                    }

                    @Override
                    public void onWriteFail() {

                    }
                });
                if (pd.isShowing()) {
                    pd.dismiss();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pd.setMessage("Connected to AmbiCube\nUploading Beacon Information...");
                        pd.setIndeterminate(true);
                        pd.setCancelable(false);
                        pd.show();
                    }
                });
            }

            @Override
            public void onAuthenticationError() {
                Log.e(TAG, "Status: Connection to beacon fail!!!");
                if (pd.isShowing()) {
                    pd.dismiss();
                }
                showdialog("Authentication Error", "Fail to connect to beacon. Please try again");
            }

            @Override
            public void onDisconnected() {
                Log.e(TAG, "Status: Beacon Disconnected!!!");
                if (pd.isShowing()) {
                    pd.dismiss();
                }
                showdialog("Disconnected", "Beacon Disconnected");
            }

            @Override
            public void onSensorData(final byte[] FlashData) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.e(TAG, "Status: onSensorData");
                        StringBuilder sb1 = new StringBuilder(FlashData.length * 2);
                        for (byte b : FlashData)
                            sb1.append(String.format("%02x", b));
                        Log.e(TAG, "onSensorData: " + sb1.toString());

                        if (allowed) {
                            if (location != null) {
                                Log.e(TAG, "lat lng: " + location.getLatitude() + "," + location.getLongitude());
                            }
                        }

                        int i0 = Integer.parseInt(String.format("%02x", FlashData[0]), 16);
                        int i1 = Integer.parseInt(String.format("%02x", FlashData[1]), 16);
                        int i2 = Integer.parseInt(String.format("%02x", FlashData[2]), 16);
                        int i3 = Integer.parseInt(String.format("%02x", FlashData[3]), 16);
                        int i4 = Integer.parseInt(String.format("%02x", FlashData[4]), 16);
                        int i5 = Integer.parseInt(String.format("%02x", FlashData[5]), 16);
                        int i6 = Integer.parseInt(String.format("%02x", FlashData[6]), 16);
                        int i7 = Integer.parseInt(String.format("%02x", FlashData[7]), 16);
                        int i8 = Integer.parseInt(String.format("%02x", FlashData[8]), 16);
                        int i9 = Integer.parseInt(String.format("%02x", FlashData[9]), 16);
                        int i10 = Integer.parseInt(String.format("%02x", FlashData[10]), 16);
                        int i11 = Integer.parseInt(String.format("%02x", FlashData[11]), 16);
                        int i12 = Integer.parseInt(String.format("%02x", FlashData[12]), 16);
                        int i13 = Integer.parseInt(String.format("%02x", FlashData[13]), 16);
                        int i14 = Integer.parseInt(String.format("%02x", FlashData[14]), 16);
                        int i15 = Integer.parseInt(String.format("%02x", FlashData[15]), 16);
                        int i16 = Integer.parseInt(String.format("%02x", FlashData[16]), 16);
                        int i17 = Integer.parseInt(String.format("%02x", FlashData[17]), 16);
                        int i18 = Integer.parseInt(String.format("%02x", FlashData[18]), 16);
                        int i19 = Integer.parseInt(String.format("%02x", FlashData[19]), 16);

                        double temperature1 = 0;
                        double humidity1 = 0;
                        double temperature2 = 0;
                        double humidity2 = 0;
                        double temperature3 = 0;
                        double humidity3 = 0;
                        double temperature4 = 0;
                        double humidity4 = 0;
                        double temperature5 = 0;
                        double humidity5 = 0;

                        if (i0 != 0xFF && i1 != 0xFF && i2 != 0xFF) {
                            counter++;
                            temperature1 = -46.85 + 175.72 * (i0 * 256 + i1) / 65536;
                            humidity1 = -6 + 125 * (i2 * 256 + i3) / 65536;

                            if (i4 != 0xFF && i5 != 0xFF && i6 != 0xFF) {
                                counter++;
                                temperature2 = -46.85 + 175.72 * (i4 * 256 + i5) / 65536;
                                humidity2 = -6 + 125 * (i6 * 256 + i7) / 65536;

                                if (i8 != 0xFF && i9 != 0xFF && i10 != 0xFF) {
                                    counter++;
                                    temperature3 = -46.85 + 175.72 * (i8 * 256 + i9) / 65536;
                                    humidity3 = -6 + 125 * (i10 * 256 + i11) / 65536;

                                    if (i12 != 0xFF && i13 != 0xFF && i14 != 0xFF) {
                                        counter++;
                                        temperature4 = -46.85 + 175.72 * (i12 * 256 + i13) / 65536;
                                        humidity4 = -6 + 125 * (i14 * 256 + i15) / 65536;

                                        if (i16 != 0xFF && i17 != 0xFF && i18 != 0xFF) {
                                            counter++;
                                            temperature5 = -46.85 + 175.72 * (i16 * 256 + i17) / 65536;
                                            humidity5 = -6 + 125 * (i18 * 256 + i19) / 65536;

                                        } else {
                                            syncfinished = true;
                                        }
                                    } else {
                                        syncfinished = true;
                                    }
                                } else {
                                    syncfinished = true;
                                }
                            } else {
                                syncfinished = true;
                            }
                        } else {
                            syncfinished = true;
                        }
                        new uploadDB(new uploadDB.returnResult() {
                            @Override
                            public void onFinish(String result) {
                                if (result != null) {
                                    Log.e(TAG, "result: " + result);
                                }
                                if (syncfinished) {
                                    new uploadDB(new uploadDB.returnResult() {
                                        @Override
                                        public void onFinish(String result) {
                                            if (result != null) {
                                                if (result.equalsIgnoreCase("Success counter")) {
                                                    if (pd.isShowing()) {
                                                        pd.dismiss();
                                                    }
                                                    if (connection.isConnected()) {
                                                        connection.disconnect();
                                                    }
                                                    Log.e(TAG, uuid);

                                                    showdialog("Upload", "Sensor data uploaded successfully");
                                                }
                                            }
                                        }
                                    }).execute("updatecounter", mac, name, String.valueOf(counter)
                                    );
                                    syncfinished = false;
                                    counter = 0;
                                }

                            }
                        })  .execute("updatedb",
                                mac,
                                name,
                                String.format(Locale.getDefault(), "%.2f", temperature1),
                                String.format(Locale.getDefault(), "%.2f", humidity1),
                                String.format(Locale.getDefault(), "%.2f", temperature2),
                                String.format(Locale.getDefault(), "%.2f", humidity2),
                                String.format(Locale.getDefault(), "%.2f", temperature3),
                                String.format(Locale.getDefault(), "%.2f", humidity3),
                                String.format(Locale.getDefault(), "%.2f", temperature4),
                                String.format(Locale.getDefault(), "%.2f", humidity4),
                                String.format(Locale.getDefault(), "%.2f", temperature5),
                                String.format(Locale.getDefault(), "%.2f", humidity5),
                                uuid,
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude())
                        );
                    }
                });

            }

        };
    }

    public void showdialog(final String title, final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog ad = new AlertDialog.Builder(MainActivity.this)
                        .setTitle(title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                ad.show();
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            this.location = location;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Utils.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            } else {
                checkBluetoothConnection();
            }
        } else if (requestCode == Utils.REQUEST_ENABLE_LOCATION) {
            if (resultCode != RESULT_OK) {
                Utils.onGPS(this);
            }
        }
    }


    private void checkBluetoothConnection() {
        if (bt_Adapter == null || !bt_Adapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), Utils.REQUEST_ENABLE_BT);
        }
    }


    private boolean isGooglePlayServicesAvailable() {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.d(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        Log.d(TAG, "This device is supported.");
        return true;
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utils.REQUEST_ENABLE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    allowed = true;
                    if (isGooglePlayServicesAvailable()) {
                        locationRequest = new LocationRequest();
                        locationRequest.setInterval(2 * 1000);
                        locationRequest.setFastestInterval(2000);
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        mGoogleApiClient = new GoogleApiClient.Builder(this)
                                .addConnectionCallbacks(this)
                                .addOnConnectionFailedListener(this)
                                .addApi(LocationServices.API)
                                .build();
                    }
                }
            }
        }
        if (!allowed) {
            AlertDialog ad = new AlertDialog.Builder(this)
                    .setTitle("Access to Location Permission")
                    .setMessage("This app needs Location permission to detect nearby beacons")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Utils.REQUEST_ENABLE_LOCATION);
                        }
                    })
                    .create();
            ad.show();
        }
    }


    public void onConnected(@Nullable Bundle bundle) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }


    public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended: googleApiClient.connect()");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: connectionResult.toString() = " + connectionResult.toString());
    }

    //add selected beacon into an Object ArrayList. HashMap is used to properly map each data to the correct beacon.
    public static void addBarrel(String name, String mac, String uuid) {
        Barrels barrels = new Barrels();
        Map<String, Barrels> barrelMaps = new HashMap<>();
        barrels.setBarrelName(name);
        barrels.setBeaconMAC(mac);
        barrels.setBeaconUUID(uuid);

        if(!barrelMaps.containsKey(mac))
        {
            barrelMaps.put(mac, barrels);
        }
        barrelList.addAll(barrelMaps.values());
    }

    //save Object ArrayList into sharedPreference. Using Gson to convert them into Json string
    public static void saveData(Context dan) {
        SharedPreferences sharedPreferences = dan.getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(barrelList);
        editor.putString("barrel list", json);
        editor.apply();
    }

    //load saved Json string from sharedPreference into an Object ArrayList
    public static void loadData(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("barrel list", null);
        Type type = new TypeToken<ArrayList<Barrels>>() {}.getType();
        barrelList = gson.fromJson(json, type);

        if(barrelList == null) {
            barrelList = new ArrayList<>();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        recyclerView = findViewById(R.id.barrel_rc);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadData(getApplicationContext());

        adapter = new BarrelAdapter(this, barrelList, this);
        recyclerView.setAdapter(adapter);

        if(adapter.getItemCount() == 0)
        {
            emptyText.setVisibility(View.VISIBLE);
        }
        else
        {
            emptyText.setVisibility(View.GONE);
        }

    }

    //open up beacon page to search for BLE beacons
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, BeaconList.class);
        startActivity(intent);
    }

    //Upload selected beacon data to the server
    @Override
    public void onDownloadClick(int position) {

        if(isOnline())
        {
            pingServer();
            if(serverConnection == true)
            {
                connection = new BarrelConnection(this, barrelList.get(position), connectionCallback());
                if (!connection.isConnected()) {
                    connection.connectBeaconWithPassword("666666");
                    pd.setMessage("Trying to connect to AmbiCube\n" + "\n" + barrelList.get(position).getBeaconMAC());
                    pd.setIndeterminate(true);
                    pd.show();
                } else {
                    showdialog("Server", "Connection to server failed.");
                }
            }
        } else {
            showdialog("Internet", "Internet connection is needed to perform certain actions.");
        }
    }

    //Retrieve selected beacon history from server
    @Override
    public void onHistoryClick(final int position) {

       if(isOnline())
       {
           pingServer();
           if(serverConnection == true)
           {
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       pd.setMessage("Retrieving History Information...");
                       pd.setIndeterminate(true);
                       pd.setCancelable(false);
                       pd.show();
                   }
               });
               new uploadDB(new uploadDB.returnResult() {
                   @Override
                   public void onFinish(String result) {
                       if(result != null)
                       {
                           Intent intent = new Intent(MainActivity.this, HistoryYearActivity.class);
                           intent.putExtra("barrelUUID", barrelList.get(position).getBeaconUUID());
                           intent.putExtra("barrelName", barrelList.get(position).getBarrelName());
                           intent.putExtra("result", result);
                           startActivity(intent);
                           pd.dismiss();
                       }
                   }
               }).execute("historypageall",barrelList.get(position).getBeaconUUID());
           } else {
               showdialog("Server", "Connection to server failed.");
           }
       } else {
           showdialog("Internet", "Internet connection is needed to perform certain actions.");
       }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle("Quit App")
                .setMessage("Do you want to exit the app?")
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ad.getButton(DialogInterface.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ad.dismiss();
                                finish();
                            }
                        });
            }
        });
        ad.show();
    }

    interface BluetoothService {
        void update(BluetoothGattCharacteristic paramBluetoothGattCharacteristic);

        void onCharacteristicWrite(BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int state);
    }

    interface ConnectionCallback {
        void onAuthenticated(Barrels barrel);

        void onAuthenticationError();

        void onDisconnected();

        void onSensorData(byte[] FlashData);
    }

}












