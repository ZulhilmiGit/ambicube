package com.tic.beacon;

//Model class for graph data
public class TempHumid implements Comparable<TempHumid> {

    private String Calendar;
    private String Temperature;
    private String Humidity;
    private String Week;

    public String getCalendar() {
        return Calendar;
    }

    public void setCalendar(String calendar) {
        Calendar = calendar;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String temperature) {
        Temperature = temperature;
    }

    public String getHumidity() {
        return Humidity;
    }

    public void setHumidity(String humidity) {
        Humidity = humidity;
    }

    public String getWeek() { return Week; }

    public void setWeek(String week) { Week = week; }


    //sort date in ascending order
    @Override
    public int compareTo(TempHumid o) {
        return getCalendar().compareTo(o.getCalendar());
    }
}
