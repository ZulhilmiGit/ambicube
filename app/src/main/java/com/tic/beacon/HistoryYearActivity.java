package com.tic.beacon;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HistoryYearActivity extends AppCompatActivity implements
        View.OnClickListener {

    ListView listView;
    JSONArray jsonArray;

    String result;
    String uuid;
    String name;

    ArrayList<String> yearList = new ArrayList<>();
    static ArrayList<TempHumid> tempHumido = new ArrayList<>();
    private static final String TAG = HistoryActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_year);

        listView = findViewById(R.id.history_year);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        uuid = intent.getStringExtra("barrelUUID");
        name = intent.getStringExtra("barrelName");

        findViewById(R.id.graph_view).setOnClickListener(this);

        try {
            parsetoListView(result);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String year = (String) listView.getItemAtPosition(position);
                Intent myIntent = new Intent(HistoryYearActivity.this, HistoryMonthActivity.class);
                myIntent.putExtra("Year", year);
                myIntent.putExtra("barrelUUID", uuid);
                myIntent.putExtra("barrelName", name);
                myIntent.putExtra("result", result);
                startActivity(myIntent);
            }
        });
    }


    //open graph page
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(HistoryYearActivity.this, Graphs.class);
        intent.putExtra("barrelUUID", uuid);
        intent.putExtra("barrelName", name);
        intent.putExtra("result", result);
        intent.putExtra("activityName", "HistoryYearActivity");
        startActivity(intent);
    }

    //parse data to List View
    public void parsetoListView(String json) throws JSONException, ParseException {

        jsonArray = new JSONArray(json);
        Calendar cal = Calendar.getInstance();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            String datetime = obj.getString("created"); //get datetime in Json string
            String dato = datetime.replaceAll("-","/"); //replcae '-' in datetime string to '/'
            Date pls = new SimpleDateFormat("yyyy/MM/dd").parse(dato); //convert datetime string to Date
            cal.setTime(pls);
            int week = cal.get(Calendar.WEEK_OF_MONTH); //get the week of the month of Date
            String cb = String.valueOf(week); //get string value of int week
            String[] s = datetime.split(" ", 2); // split the date and time from datetime string and add into Array
            String date = s[0]; //get date value from Array
            String year = date.substring(0,4); //get the year from date string
            String tem = obj.getString("temp_sensor"); //get temperature from Json string
            String hum = obj.getString("hum_sensor"); //get humidity from Json string
            addToMap(datetime, tem, hum, cb); // add to Object ArrayList
            yearList.add(year); // add to String ArrayList
        }

        Collections.sort(tempHumido);
        Set<String> uniqueYear = new HashSet<>(yearList);
        List<String> list = new ArrayList<>(uniqueYear);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(arrayAdapter);
    }

    public void addToMap(String date, String temp, String humid, String Week) {

        TempHumid tempHumid = new TempHumid();
        Map<String, TempHumid> tempHumidMaps = new HashMap<>();

        tempHumid.setCalendar(date);
        tempHumid.setTemperature(temp);
        tempHumid.setHumidity(humid);
        tempHumid.setWeek(Week);

        if(!tempHumidMaps.containsKey(date))
        {
            tempHumidMaps.put(date, tempHumid);
        }

        tempHumido.addAll(tempHumidMaps.values());
    }

    //return Object ArrayList
    public static ArrayList<TempHumid> getAll()
    {
        return tempHumido;
    }

    @Override
    public void onBackPressed() {
        //clear temperature and humidity Object ArrayList
        tempHumido.clear();
        this.finish();
    }
}


