package com.tic.beacon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

//custom List View adapter
public class HistoryAdapter extends ArrayAdapter<History>{

    public ArrayList<History> dataSet;
    Context mContext;
    static int item = 100;

    private static class ViewHolder {
        TextView Date;
        TextView Time;
        TextView Temperature;
        TextView Humidity;
    }

    public HistoryAdapter(ArrayList<History> data, Context context) {
        super(context, R.layout.history_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        History dataModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.history_item, parent, false);
            viewHolder.Date = convertView.findViewById(R.id.date);
            viewHolder.Time = convertView.findViewById(R.id.time);
            viewHolder.Temperature = convertView.findViewById(R.id.temperature);
            viewHolder.Humidity = convertView.findViewById(R.id.humidity);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.Date.setText(dataModel.getDate());
        viewHolder.Time.setText(dataModel.getTime());
        viewHolder.Temperature.setText(dataModel.getTemperature());
        viewHolder.Humidity.setText(dataModel.getHumidity());

        return convertView;
    }

    @Override
    public int getCount() { return dataSet.size() < item ? dataSet.size() : item; }

    //increase the amount of list view items to return when user clicks load more button
    public static int increaseCount()
    {
        item = item + 100;
        return item;
    }

    //reset item count
    public static int resetCount()
    {
        item = 100;
        return item;
    }

    public static int getItem() { return item; }
}
