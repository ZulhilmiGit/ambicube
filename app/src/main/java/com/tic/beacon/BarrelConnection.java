package com.tic.beacon;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.tic.beacon.MainActivity.ConnectionCallback;
import com.tic.beacon.MainActivity.BluetoothService;

//same as beaconconnecton. use to connect to saved beacon
class BarrelConnection {

    private static final String TAG = BarrelConnection.class.getSimpleName();

    static final UUID JAALEE_BEACON_SERVICE = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
    static final UUID BEACON_KEEP_CONNECT_CHAR = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");
    static final UUID BEACON_UUID_CHAR = UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb");
    static final UUID MAJOR_CHAR = UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb");
    static final UUID MINOR_CHAR = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb");
    static final UUID POWER_CHAR = UUID.fromString("0000fff5-0000-1000-8000-00805f9b34fb");
    static final UUID BEACON_FFF6 = UUID.fromString("0000fff6-0000-1000-8000-00805f9b34fb");
    static final UUID BEACON_FFF7 = UUID.fromString("0000fff7-0000-1000-8000-00805f9b34fb");
    static final UUID BEACON_FFF8 = UUID.fromString("0000fff8-0000-1000-8000-00805f9b34fb");

    static final UUID BEACON_STATE_SERVICE = UUID.fromString("0000ff70-0000-1000-8000-00805f9b34fb");

    static final UUID SHT_SENSOR_SERVICE_UUID = UUID.fromString("0000ffb0-0000-1000-8000-00805f9b34fb");
    static final UUID SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT = UUID.fromString("0000ffb4-0000-1000-8000-00805f9b34fb");//stored data
    static final UUID SHT_SENSOR_CHARACTERISTIC_DATA_SYNC = UUID.fromString("0000ffb5-0000-1000-8000-00805f9b34fb");//start notify stored data

    private final Map<UUID, MainActivity.BluetoothService> uuidToService;
    private LinkedList<BluetoothGattCharacteristic> toFetch = new LinkedList<>();


    private final Context context;
    private final BluetoothDevice device;
    private final ConnectionCallback connectionCallback;
    private final Handler handler;
    private final BluetoothGattCallback bluetoothGattCallback;

    private BluetoothGatt mBluetoothGatt;

    private int m_time_offect = 0;

    byte[] FlashData = new byte[0];

    private String mPassword;
    private boolean mStartWritePass = false;
    private boolean didReadCharacteristics;
    private boolean mPassWordWriteSuccess = false;
    private boolean deviceConnected = false;
    private boolean mCurrentIsJaaleeNewBeacon = false;

    private final JaaleeServices mBeaconService;

    BarrelConnection(Context context, Barrels bb, MainActivity.ConnectionCallback connectionCallback) {
        this.context = context;
        this.device = deviceFromBeacon(bb);
        this.handler = new Handler();
        this.connectionCallback = connectionCallback;
        this.bluetoothGattCallback = blegattCallback();
        this.mBeaconService = new JaaleeServices();

        mCurrentIsJaaleeNewBeacon = false;

        uuidToService = new HashMap<>();
        uuidToService.put(JAALEE_BEACON_SERVICE, mBeaconService);

    }

    private BluetoothDevice deviceFromBeacon(Barrels barrel) {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        return bluetoothAdapter.getRemoteDevice(barrel.getBeaconMAC());
    }

    private BluetoothGattCallback blegattCallback() {
        return new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                switch (newState) {
                    case BluetoothProfile.STATE_DISCONNECTED:
                        Log.e(TAG, "Disconnected from GATT server.");
                        deviceConnected = false;
                        connectionCallback.onAuthenticationError();
                        connectionCallback.onDisconnected();
                        break;

                    case BluetoothProfile.STATE_CONNECTING:
                        Log.e(TAG, "Connecting to GATT server");
                        deviceConnected = false;
                        connectionCallback.onAuthenticationError();
                        break;

                    case BluetoothProfile.STATE_CONNECTED:
                        Log.e(TAG, "Connected to GATT server.");
                        deviceConnected = true;
                        mBluetoothGatt = gatt;
                        mBluetoothGatt.discoverServices();
                        break;
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                Log.e(TAG, "onCharacteristicChanged" + characteristic);
                if (characteristic.getUuid().equals(SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT)) {
                    Log.e(TAG, "onCharacteristicChanged read time offset success");
                    m_time_offect = Utils.getUnsignedByte(characteristic.getValue());
                } else if (characteristic.getUuid().equals(SHT_SENSOR_CHARACTERISTIC_DATA_SYNC)) {
                    Log.e(TAG, "onCharacteristicChanged HandleReceivedDataFromSensor read data_sync success");
                    FlashData = null;
                    FlashData = new byte[0];
                    HandleReceivedDataFromSensor(characteristic.getValue());
                }
            }

            @Override
            public void onCharacteristicRead(final BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                Log.e(TAG, "onCharacteristicRead:");
                if (status == 0) {
                    mStartWritePass = characteristic.getValue().length == 3;
                    if (characteristic.getUuid().equals(BEACON_KEEP_CONNECT_CHAR)) {
                        if (!mPassWordWriteSuccess) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e(TAG, "Writing Password");
                                    BeaconKeepConnect(new WriteCallBack() {
                                        @Override
                                        public void onWriteSuccess() {
                                            Log.e(TAG, "Writing Password Success");
                                            mPassWordWriteSuccess = true;
                                        }

                                        @Override
                                        public void onWriteFail() {
                                            Log.e(TAG, "Writing Password Fail");
                                        }
                                    });
                                }
                            }, TimeUnit.SECONDS.toMillis(1L));

                        }
                    }
                    BluetoothService temp = BarrelConnection.this.uuidToService
                            .get(characteristic.getService().getUuid());

                    if (temp != null) {
                        temp.update(characteristic);
                    }
                    if (mPassWordWriteSuccess) {
                        readCharacteristics(gatt);
                    }

                    if (characteristic.getService().getUuid().equals(SHT_SENSOR_SERVICE_UUID)) {
                        Log.e(TAG, "onCharacteristicRead SHT_SENSOR_SERVICE_UUID:" + characteristic.getUuid().toString() );
                        Read_Sensor_Time_Offect();
                        if (characteristic.getUuid().equals(SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT)) {
                            m_time_offect = Utils.getUnsignedByte(characteristic.getValue());
                            Log.e(TAG, "onCharacteristicRead read time offset success............." + String.valueOf(m_time_offect));
                        } else if (characteristic.getUuid().equals(SHT_SENSOR_CHARACTERISTIC_DATA_SYNC)) {
                            Log.e(TAG, "onCharacteristicRead HandleReceivedDataFromSensor read data_sync success-> "
                                    + Arrays.toString(characteristic.getValue()));
                            Start_Sync_Saved_Sensor_data();
                            //HandleReceivedDataFromSensor(characteristic.getValue());
                        }
                    }
                } else {
                    toFetch.clear();
                    connectionCallback.onAuthenticationError();
                }
            }

            @Override
            public void onCharacteristicWrite(final BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                Log.e(TAG, "onCharacteristicWrite: " + characteristic);
                BluetoothService temp = uuidToService.get(characteristic.getService().getUuid());
                if (temp != null) {
                    temp.onCharacteristicWrite(characteristic, status);
                }

                if (BEACON_KEEP_CONNECT_CHAR.equals(characteristic.getUuid())) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            readCharacteristics(gatt);
                        }
                    }, 500L);
                }
            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                Log.e(TAG, "onServicesDiscovered");
                if (status == 0) {
                    processDiscoveredServices(gatt.getServices());
                    for (BluetoothGattService service : gatt.getServices()) {
                        if (BEACON_STATE_SERVICE.equals(service.getUuid())) {
                            mCurrentIsJaaleeNewBeacon = true;
                            JaaleeServices.mCurrentIsJaaleeNewBeacon = true;
                        } else if (JAALEE_BEACON_SERVICE.equals(service.getUuid())) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    BluetoothGattCharacteristic keepChar = mBeaconService.getKeepUUIDChar();
                                    mBluetoothGatt.readCharacteristic(keepChar);
                                }
                            }, TimeUnit.SECONDS.toMillis(1L));

                        }
                    }
                } else {
                    connectionCallback.onAuthenticationError();
                }
            }
        };
    }

    void connectBeaconWithPassword(String password) {
        mPassword = password;
        Log.e(TAG, "Trying to connect to GATT");
        JaaleeServices.mCurrentIsJaaleeNewBeacon = false;
        didReadCharacteristics = true;
        mPassWordWriteSuccess = false;

        mStartWritePass = false;
        mBluetoothGatt = device.connectGatt(context, false, bluetoothGattCallback);
    }

    void setTemp(boolean state, WriteCallBack writeCallback) {

        if (!isConnected()) {
            Log.e(TAG, "Failed to keep connection");
            writeCallback.onWriteFail();
            return;
        }

        BluetoothGattService tempService = mBluetoothGatt.getService(SHT_SENSOR_SERVICE_UUID);
        if (tempService == null) {
            Log.e(TAG, "Current beacon does not find acceleration service ... sensor service ");
            writeCallback.onWriteFail();
            return;
        }

        BluetoothGattCharacteristic tempChar = tempService.getCharacteristic(SHT_SENSOR_CHARACTERISTIC_DATA_SYNC);
        if (tempChar == null) {
            Log.e(TAG, "Current beacon does not find motion notification characteristic .... data sync");
            writeCallback.onWriteFail();
            return;
        }

        BluetoothGattDescriptor tempDescriptor = tempChar.getDescriptors().get(0);

        if (state) {
            mBluetoothGatt.setCharacteristicNotification(tempChar, true);
            tempDescriptor
                    .setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        } else {
            mBluetoothGatt.setCharacteristicNotification(tempChar, false);
            tempDescriptor
                    .setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        }

        if (mBluetoothGatt.writeDescriptor(tempDescriptor)) {
            writeCallback.onWriteSuccess();
        } else {
            writeCallback.onWriteFail();
        }
    }

    private void BeaconKeepConnect(WriteCallBack writeCallback) {
        if (!isConnected() || !mBeaconService.hasCharacteristic(BEACON_KEEP_CONNECT_CHAR)) {
            Log.e(TAG, "Failed to keep connection");
            writeCallback.onWriteFail();
            return;
        }

        BluetoothGattCharacteristic uuidChar = mBeaconService.beforeCharacteristicWrite(BEACON_KEEP_CONNECT_CHAR, writeCallback);

        byte[] arrayOfByte1 = new byte[]{(byte) 0x66, (byte) 0x66, (byte) 0x66};
        uuidChar.setValue(arrayOfByte1);

        StringBuilder sb1 = new StringBuilder();
        for (byte a : arrayOfByte1) {
            sb1.append(String.format("%02x", a));
        }
        Log.e(TAG, "password: " + sb1.toString());

        mBluetoothGatt.writeCharacteristic(uuidChar);
    }

    private void HandleReceivedDataFromSensor(byte[] data) {
        for (int i = 0; i < data.length; ) {
            byte[] TEMP = Utils.subBytes(data, i, 4);
            FlashData = Utils.addBytes(FlashData, TEMP);
            i += 4;
        }
        Log.e(TAG, "HandleReceivedDataFromSensor String");
        StringBuilder sb1 = new StringBuilder(FlashData.length * 2);
        for (byte b : FlashData)
            sb1.append(String.format("%02x", b));
        Log.e(TAG, "HandleReceivedDataFromSensor String: " + sb1);
        connectionCallback.onSensorData(FlashData);
        //onSensorData:73409eaa73409eba73409eaa73409eba73409eda

    }

    private void processDiscoveredServices(List<BluetoothGattService> services) {
        mBeaconService.processGattServices(services);
        toFetch.clear();
        processServerDataToRead();
    }

    private void processServerDataToRead() {
        Collection<BluetoothGattCharacteristic> temp = mBeaconService.getAvailableCharacteristics();
        if (temp != null)
            this.toFetch.addAll(temp);
    }


    private void Read_Sensor_Time_Offect() {
        Log.e(TAG, "Read_Sensor_Time_Offect");
        BluetoothGattService bluetoothGattService = mBluetoothGatt.getService(SHT_SENSOR_SERVICE_UUID);
        BluetoothGattCharacteristic OffectChar = bluetoothGattService.getCharacteristic(SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT);
        mBluetoothGatt.readCharacteristic(OffectChar);
    }

    private void Start_Sync_Saved_Sensor_data() {
        Log.e(TAG, "Start_Sync_Saved_Sensor_data");
        BluetoothGattService bluetoothGattService = mBluetoothGatt.getService(SHT_SENSOR_SERVICE_UUID);
        BluetoothGattCharacteristic syncChar = bluetoothGattService.getCharacteristic(SHT_SENSOR_CHARACTERISTIC_DATA_SYNC);
        mBluetoothGatt.setCharacteristicNotification(syncChar, true);
        BluetoothGattDescriptor syncDescriptor = syncChar.getDescriptors().get(0);
        syncDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        Log.e(TAG,"syncDescriptor: " + syncDescriptor.toString());
        if (mBluetoothGatt.writeDescriptor(syncDescriptor)) {
            Log.e(TAG, "start sync sensor data success");
        } else {
            Log.e(TAG, "start sync sensor data fail");
        }
    }

    private void readCharacteristics(BluetoothGatt gatt) {
        if (toFetch.size() != 0) {
            BluetoothGattCharacteristic temp = toFetch.poll();
            gatt.readCharacteristic(temp);

        } else if (mBluetoothGatt != null) {
            didReadCharacteristics = true;
            connectionCallback.onAuthenticated(new Barrels(mBeaconService,device));
        }
    }

    boolean isConnected() {
        BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        int connectionState = bluetoothManager.getConnectionState(device, 7);
        return (connectionState == 2);
    }

    void disconnect() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();
        }
    }

    interface WriteCallBack {
        void onWriteSuccess();

        void onWriteFail();
    }
}
