package com.tic.beacon;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

class JaaleeServices implements MainActivity.BluetoothService
{
    private final HashMap<UUID, BluetoothGattCharacteristic> characteristics = new HashMap<>();

    private final HashMap<UUID, BarrelConnection.WriteCallBack> writeCallbacks = new HashMap<>();

    static boolean mCurrentIsJaaleeNewBeacon = false;//

    void processGattServices(List<BluetoothGattService> services)
    {
        for (BluetoothGattService service : services) {
            if(BarrelConnection.SHT_SENSOR_SERVICE_UUID.equals(service.getUuid())){
                if (service.getCharacteristic(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_DATA_SYNC) != null) {
                    this.characteristics.put(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_DATA_SYNC, service.getCharacteristic(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_DATA_SYNC));
                }
                if (service.getCharacteristic(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT) != null) {
                    this.characteristics.put(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT, service.getCharacteristic(BarrelConnection.SHT_SENSOR_CHARACTERISTIC_TIME_OFFECT));
                }
            }
            if (BarrelConnection.JAALEE_BEACON_SERVICE.equals(service.getUuid())) {

                if (service.getCharacteristic(BarrelConnection.BEACON_KEEP_CONNECT_CHAR) != null) {
                    this.characteristics.put(BarrelConnection.BEACON_KEEP_CONNECT_CHAR, service.getCharacteristic(BarrelConnection.BEACON_KEEP_CONNECT_CHAR));
                }

                if (service.getCharacteristic(BarrelConnection.BEACON_UUID_CHAR) != null) {
                    this.characteristics.put(BarrelConnection.BEACON_UUID_CHAR, service.getCharacteristic(BarrelConnection.BEACON_UUID_CHAR));
                }

                if (service.getCharacteristic(BarrelConnection.MAJOR_CHAR) != null) {
                    this.characteristics.put(BarrelConnection.MAJOR_CHAR, service.getCharacteristic(BarrelConnection.MAJOR_CHAR));
                }

                if (service.getCharacteristic(BarrelConnection.MINOR_CHAR) != null) {
                    this.characteristics.put(BarrelConnection.MINOR_CHAR, service.getCharacteristic(BarrelConnection.MINOR_CHAR));
                }

                if (service.getCharacteristic(BarrelConnection.POWER_CHAR) != null) {
                    this.characteristics.put(BarrelConnection.POWER_CHAR, service.getCharacteristic(BarrelConnection.POWER_CHAR));
                }

                if (service.getCharacteristic(BarrelConnection.BEACON_FFF6) != null) {
                    this.characteristics.put(BarrelConnection.BEACON_FFF6, service.getCharacteristic(BarrelConnection.BEACON_FFF6));
                }

                if (service.getCharacteristic(BarrelConnection.BEACON_FFF7) != null) {
                    this.characteristics.put(BarrelConnection.BEACON_FFF7, service.getCharacteristic(BarrelConnection.BEACON_FFF7));
                }

                if (service.getCharacteristic(BarrelConnection.BEACON_FFF8) != null) {
                    this.characteristics.put(BarrelConnection.BEACON_FFF8, service.getCharacteristic(BarrelConnection.BEACON_FFF8));
                }
            }
        }
    }

    boolean hasCharacteristic(UUID uuid)
    {
        return this.characteristics.containsKey(uuid);
    }

    String getBeaconUUID()
    {
        String TEMP = this.characteristics.containsKey(BarrelConnection.BEACON_UUID_CHAR) ?
                getStringValue(this.characteristics.get(BarrelConnection.BEACON_UUID_CHAR).getValue()) : null;

        if (TEMP != null)
        {
            TEMP = normalizeProximityUUID(TEMP);
            TEMP = TEMP.toUpperCase();
        }

        return TEMP;
    }

    private String normalizeProximityUUID(String proximityUUID) {
        String withoutNull = proximityUUID.replace(" ", "").toLowerCase();
        String withoutDashes = withoutNull.replace("-", "").toLowerCase();

        return String.format(
                "%s-%s-%s-%s-%s",
                withoutDashes.substring(0, 8),
                withoutDashes.substring(8, 12),
                withoutDashes.substring(12, 16),
                withoutDashes.substring(16, 20),
                withoutDashes.substring(20, 32));
    }

    int getBeaconMajor()
    {
        return this.characteristics.containsKey(BarrelConnection.MAJOR_CHAR) ?
                getUnsignedInt16(this.characteristics.get(BarrelConnection.MAJOR_CHAR).getValue()) : null;
    }

    int getBeaconMinor()
    {
        return this.characteristics.containsKey(BarrelConnection.MINOR_CHAR) ?
                getUnsignedInt16(this.characteristics.get(BarrelConnection.MINOR_CHAR).getValue()) : null;
    }

    int getBeaconPower()
    {
        return this.characteristics.containsKey(BarrelConnection.POWER_CHAR) ?
                getUnsignedByte(this.characteristics.get(BarrelConnection.POWER_CHAR).getValue()) : null;
    }

    public int getBeaconMfgr()
    {
        if (mCurrentIsJaaleeNewBeacon)
        {
            return this.characteristics.containsKey(BarrelConnection.BEACON_FFF7) ?
                    getUnsignedInt16(this.characteristics.get(BarrelConnection.BEACON_FFF7).getValue()) : null;
        }
        return 0;
    }

    public int getBeaconBroadcastInterval()
    {
        if (mCurrentIsJaaleeNewBeacon)
        {
            return this.characteristics.containsKey(BarrelConnection.BEACON_FFF6) ?
                    getUnsignedByte(this.characteristics.get(BarrelConnection.BEACON_FFF6).getValue()) : null;
        }
        else
        {
            return this.characteristics.containsKey(BarrelConnection.BEACON_FFF7) ?
                    getUnsignedByte(this.characteristics.get(BarrelConnection.BEACON_FFF7).getValue()) : null;
        }
    }

    public void update(BluetoothGattCharacteristic characteristic)
    {
        this.characteristics.put(characteristic.getUuid(), characteristic);

        if (!mCurrentIsJaaleeNewBeacon)
        {
            byte [] Value = characteristic.getValue();

            BarrelConnection.WriteCallBack writeCallback = writeCallbacks.remove(characteristic.getUuid());
            if (writeCallback != null)
            {
                if (Value[0] == 1)
                {
                    writeCallback.onWriteSuccess();
                }
                else
                {
                    writeCallback.onWriteFail();
                }
            }
        }

    }

    Collection<BluetoothGattCharacteristic> getAvailableCharacteristics() {
        if (this.characteristics.size() == 0)
        {
            return null;
        }
        List<BluetoothGattCharacteristic> chars = new ArrayList<BluetoothGattCharacteristic>(this.characteristics.values());
        chars.removeAll(Collections.singleton(null));
        return chars;
    }

    public BluetoothGattCharacteristic getAvailableCharacteristic(UUID uuid) {
        return this.characteristics.get(uuid);
    }


    BluetoothGattCharacteristic getKeepUUIDChar() {
        return this.characteristics.get(BarrelConnection.BEACON_KEEP_CONNECT_CHAR);
    }

    BluetoothGattCharacteristic beforeCharacteristicWrite(UUID uuid, BarrelConnection.WriteCallBack callback) {
        this.writeCallbacks.put(uuid, callback);
        return this.characteristics.get(uuid);
    }

    public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, int status) {
        if (mCurrentIsJaaleeNewBeacon)
        {
            BarrelConnection.WriteCallBack writeCallback = writeCallbacks.remove(characteristic.getUuid());
            if (status == 0)
                writeCallback.onWriteSuccess();
            else
                writeCallback.onWriteFail();
        }
        else if (BarrelConnection.BEACON_KEEP_CONNECT_CHAR.equals(characteristic.getUuid()))
        {
            BarrelConnection.WriteCallBack writeCallback = writeCallbacks.remove(characteristic.getUuid());
            if (status == 0)
                writeCallback.onWriteSuccess();
            else
                writeCallback.onWriteFail();
        }

    }

    private static String getStringValue(byte[] bytes) {
        String stmp;
        StringBuilder sb = new StringBuilder("");
        for (byte aByte : bytes) {
            stmp = Integer.toHexString(aByte & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
            sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }

    private static int getUnsignedByte(byte[] bytes) {
        return unsignedByteToInt(bytes[0]);
    }

    private static int unsignedByteToInt(byte value)
    {
        return value & 0xFF;
    }

    private static int getUnsignedInt16(byte[] bytes) {
        return unsignedByteToInt(bytes[1]) + (unsignedByteToInt(bytes[0]) << 8);
    }
}
