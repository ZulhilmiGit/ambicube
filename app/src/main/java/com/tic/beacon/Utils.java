package com.tic.beacon;
import android.app.Activity;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import java.util.Locale;
import java.util.UUID;

class Utils {
    static final int REQUEST_ENABLE_LOCATION = 0x98;
    static final int REQUEST_ENABLE_BT = 0x99;

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    static UUID bytesToUuid(@NonNull final byte[] bytes) {
        final char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            final int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }

        final String hex = new String(hexChars);

        return UUID.fromString(hex.substring(0, 8) + "-" +
                hex.substring(8, 12) + "-" +
                hex.substring(12, 16) + "-" +
                hex.substring(16, 20) + "-" +
                hex.substring(20, 32));
    }

    static byte[] UuidToByteArray(@NonNull final UUID uuid) {
        final String hex = uuid.toString().replace("-", "");
        final int length = hex.length();
        final byte[] result = new byte[length / 2];

        for (int i = 0; i < length; i += 2) {
            result[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
        }

        return result;
    }

    static byte[] integerToByteArray(final int value) {
        final byte[] result = new byte[2];
        result[0] = (byte) (value / 256);
        result[1] = (byte) (value % 256);

        return result;
    }

    static String getTemp(int Major, int Minor){
        int Temperature = ((Major & 0x00FF) << 8) | ((Minor & 0xC000) >> 8);

        float RealTemperature = (float) (-46.85 + 175.72 * (Temperature / 65536.0f));
        return String.format(Locale.getDefault(),"%.2f", RealTemperature);
    }

    static String getHum(int Major){
        int Humidity = (Major & 0xFF00);// >> 8;
        float RealHumidity = -6 + 125 * (Humidity / 65536.0f);
        return String.format(Locale.getDefault(),"%.2f",RealHumidity);
    }

    static String computeAccuracy(int rssi, int power) {
        double value;
        if (rssi == 0.0D) {
            value = -1.0D;
            return String.format(Locale.getDefault(),"%.2f",value);
        }

        double ratio = rssi * 1.0D / power;
        if (ratio < 1.0D) {
            value = Math.pow(ratio, 8.0D);
            return String.format(Locale.getDefault(),"%.2f",value);
        }
        value = 0.69976D * Math.pow(ratio, 7.7095D) + 0.111D;
        return String.format(Locale.getDefault(),"%.2f",value);
    }

    static int byteArrayToInteger(final byte[] byteArray) {
        return (byteArray[0] & 0xff) * 0x100 + (byteArray[1] & 0xff);
    }

    static int getUnsignedByte(byte[] bytes) {
        return unsignedByteToInt(bytes[0]);
    }

    static byte[] addBytes(byte[] data1, byte[] data2) {
        byte[] data3 = new byte[data1.length + data2.length];
        System.arraycopy(data1, 0, data3, 0, data1.length);
        System.arraycopy(data2, 0, data3, data1.length, data2.length);
        return data3;
    }

    static byte[] subBytes(byte[] src, int begin, int count) {
        byte[] bs = new byte[count];
        System.arraycopy(src, begin, bs, 0, begin + count - begin);
        return bs;
    }

    static int decode(char ch) {
        if ((ch >= '0') && (ch <= '9')) {
            return ch - '0';
        }
        if ((ch >= 'a') && (ch <= 'f')) {
            return ch - 'a' + 10;
        }
        throw new IllegalArgumentException("Illegal hexadecimal character: " + ch);
    }

    static int unsignedByteToInt(byte value) {
        return value & 0xFF;
    }

    static void onGPS(final Activity activity) {
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog ad = new AlertDialog.Builder(activity)
                    .setTitle("Turn On Location")
                    .setMessage("Please turn on GPS Location to enable beacon")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            activity.startActivityForResult(intent, REQUEST_ENABLE_LOCATION);
                        }
                    })
                    .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            onGPS(activity);
                        }
                    })
                    .create();
            ad.show();
        }

    }

    static boolean isBleSupported(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    static ScanSettings setScanSettings() {
        return new ScanSettings.Builder()
                .setReportDelay(0)
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();
    }
}
